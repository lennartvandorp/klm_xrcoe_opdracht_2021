using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Stores all plane specific data
/// </summary>
[CreateAssetMenu(fileName = "Plane", menuName = "ScriptableObjects/PlaneObject", order = 0)]
public class PlaneObject : ScriptableObject
{
    public string brand, type;
}
