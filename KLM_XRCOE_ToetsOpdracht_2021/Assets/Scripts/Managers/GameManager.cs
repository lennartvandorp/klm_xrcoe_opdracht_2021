using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    private void Start()
    {
        if (Instance == null) { Instance = this; }
        ToggleLights();
        SetHangarNumbers();
    }


    private void Update()
    {
        UpdatePlaneControl();
    }

    #region Events
    public UnityEvent OnPlanesGoHome = new UnityEvent();
    public UnityEvent OnPlanesRoamFree = new UnityEvent();

    public class BoolEvent : UnityEvent<bool> { }
    public BoolEvent OnToggleLights = new BoolEvent();

    public void PlanesGoHome()
    {
        OnPlanesGoHome.Invoke();
    }

    public void PlanesRoamFree()
    {
        OnPlanesRoamFree.Invoke();
    }

    bool lightsOn;
    /// <summary>
    /// Turns lights off if they're on, turns them on if they're off. 
    /// </summary>
    public void ToggleLights()
    {
        OnToggleLights.Invoke(!lightsOn);
        lightsOn = !lightsOn;
    }

    #endregion

    #region Hangars
    [SerializeField] private Transform hangarParent;

    /// <summary>
    /// Collects all hangers from its parent and sets their numbers correctly
    /// </summary>
    void SetHangarNumbers()
    {
        Hangar[] hangars = hangarParent.GetComponentsInChildren<Hangar>();

        for (int i = 0; i < hangars.Length; i++)
        {
            hangars[i].SetNumber(i + 1);
        }
    }

    #endregion

    #region Controlling planes
    NavMeshAgent _controlledNavmeshAgent;
    public void SetControlledNavmeshAgent(NavMeshAgent agent)
    {
        _controlledNavmeshAgent = agent;
    }

    /// <summary>
    /// Manages setting the destination of planes manually
    /// </summary>
    private void UpdatePlaneControl()
    {
        if (_controlledNavmeshAgent == null) { return; }


        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;

            Physics.Raycast(CameraManager.CurrentCam.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity);

            var destCallback = _controlledNavmeshAgent.SetDestination(hit.point);
        }
    }


    #endregion

}