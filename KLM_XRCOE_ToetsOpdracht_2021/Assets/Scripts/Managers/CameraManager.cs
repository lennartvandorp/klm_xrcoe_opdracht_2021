using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

/// <summary>
/// Gets all cameras from the scene and automatically generates ui for it. 
/// </summary>
public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance { get; private set; }

    [SerializeField] private Transform _camBtnParent;
    [SerializeField] private GameObject _camBtn;
    [SerializeField] private float _btnDist;


    [HideInInspector] public List<Camera> _cameras;

    public static Camera CurrentCam
    {
        get
        {
            if (_currentCam == null) { return Camera.main; }
            else { return _currentCam; }
        }
    }
    private static Camera _currentCam;

    private void Start()
    {
        Instance = this;

        //get all cameras currently in the scene
        _cameras.AddRange(FindObjectsOfType<Camera>().ToList());
        UpdateCameraUI();
        SwitchToCamera(0);
    }


    /// <summary>
    /// Use this to add a camera to the manager after start. 
    /// </summary>
    /// <param name="cam"></param>
    public void AddCamera(Camera cam)
    {
        _cameras.Add(cam);
        UpdateCameraUI();
    }

    List<GameObject> _cameraButtons = new List<GameObject>();
    /// <summary>
    /// Uses the currently existing cameras to generate new camera buttons
    /// </summary>
    void UpdateCameraUI()
    {
        foreach (GameObject gameObject in _cameraButtons)
        {
            Destroy(gameObject);
        }
        for (int i = 0; i < _cameras.Count; i++)
        {
            GameObject newBtn = Instantiate(_camBtn, _camBtnParent);
            _cameraButtons.Add(newBtn);
            newBtn.transform.position = _camBtnParent.position + Vector3.down * _btnDist * i;
            newBtn.GetComponentInChildren<TextMeshProUGUI>().text = _cameras[i].name;

            int temp = i;
            Button button = newBtn.GetComponent<Button>();
            button.onClick.AddListener(() => { SwitchToCamera(temp); });

            var agent = _cameras[i].GetComponentInParent<NavMeshAgent>();
            if (agent)
            {
                button.onClick.AddListener(() => GameManager.Instance.SetControlledNavmeshAgent(agent));
            }
        }
    }

    /// <summary>
    /// Disables all but the desired camera
    /// </summary>
    /// <param name="camera"></param>
    void SwitchToCamera(int camera)
    {
        for (int i = 0; i < _cameras.Count; i++)
        {
            if (i == camera)
            {
                _currentCam = _cameras[i];
                _cameras[i].gameObject.SetActive(true);
            }
            else { _cameras[i].gameObject.SetActive(false); }
        }
    }

}

