using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class ControllablePlane : MonoBehaviour
{
    [SerializeField] private Renderer _hoverRenderer;
    private void OnMouseEnter()
    {
        _hoverRenderer.enabled = true;
    }
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown((int)MouseButton.RightMouse))
        {
            SetControllableAgent();
        }
    }
    private void OnMouseExit()
    {
        _hoverRenderer.enabled = false;
    }


    void SetControllableAgent()
    {
        GameManager.Instance.SetControlledNavmeshAgent(GetComponentInParent<NavMeshAgent>());
    }

}
