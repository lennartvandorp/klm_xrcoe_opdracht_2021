using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightToggler : MonoBehaviour
{
    private Light _light;

    // Start is called before the first frame update
    void Start()
    {
        _light = GetComponent<Light>();
        GameManager.Instance.OnToggleLights.AddListener(ToggleLight);
    }

    public void ToggleLight(bool onOrOff)
    {
        _light.enabled = onOrOff;
    }
}
