using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class Hangar : MonoBehaviour
{
    [SerializeField] private GameObject _planePrefab;
    [SerializeField] private TextMeshPro _numberText;
    [SerializeField] PlaneObject _MyPlaneObject;

    public UnityEvent OnPlaneArrivedEvent = new UnityEvent();

    int _number;

    PlaneCharacteristics _myPlane;

    private void Start()
    {
        GameObject newPlane = Instantiate(_planePrefab);
        newPlane.transform.position = transform.position;
        _myPlane = newPlane.GetComponent<PlaneCharacteristics>();
        _myPlane.SetPlaneObject(_MyPlaneObject);

        newPlane.GetComponent<PlaneMovement>().OnArrivedHome.AddListener(OnPlaneArrived);
        SetupPlane();
    }
    

    /// <summary>
    /// Run when the plane has arrived at the hangar
    /// </summary>
    private void OnPlaneArrived()
    {
        OnPlaneArrivedEvent.Invoke();
    }

    /// <summary>
    /// Used to set the number of this plane
    /// </summary>
    /// <param name="number"></param>
    public void SetNumber(int number)
    {
        _numberText.text = number.ToString();
        _number = number;
        SetupPlane();
    }


    void SetupPlane()
    {
        if (_myPlane != null)
        {
            _myPlane.SetNumber(_number);
        }
    }
}
