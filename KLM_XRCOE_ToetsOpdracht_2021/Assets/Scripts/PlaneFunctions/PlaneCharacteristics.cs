using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlaneCharacteristics : MonoBehaviour
{
    [SerializeField] private TextMeshPro _numberText, _brandText, _typeText;
    [SerializeField] private Camera _cam;
    PlaneObject _planeObject;


    /// <summary>
    /// Used to set the plane-specific information
    /// </summary>
    /// <param name="plane"></param>
    public void SetPlaneObject(PlaneObject plane)
    {
        _planeObject = plane;
        _brandText.text = _planeObject.brand;
        _typeText.text = _planeObject.type;
    }

    /// <summary>
    /// Used for logic that needs the plane's number and to set the text of the number
    /// </summary>
    /// <param name="number"></param>
    public void SetNumber(int number)
    {
        _numberText.text = number.ToString();
        if (number == 1)
        {
            CameraManager.Instance.AddCamera(_cam);
        }
    }
}
