using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class PlaneMovement : MonoBehaviour
{
    [SerializeField] float _randomRadius;
    public UnityEvent OnArrivedHome = new UnityEvent();

    private NavMeshAgent _navMeshAgent;
    private Vector3 _home;



    bool goingHome;
    private void Start()
    {
        _home = transform.position;
        _navMeshAgent = GetComponent<NavMeshAgent>();
        GoToRandomPosition();

        GameManager.Instance.OnPlanesGoHome.AddListener(GoHome);
        GameManager.Instance.OnPlanesRoamFree.AddListener(OnFreeMovement);
    }

    bool _isHome;
    private void FixedUpdate()
    {
        if (!goingHome && _navMeshAgent.velocity.magnitude == 0f)
        {
            GoToRandomPosition();
        }
        //Did they arrive on their position
        if (goingHome && !_isHome && _navMeshAgent.velocity.magnitude == 0f)
        {
            OnArrivedHome.Invoke();
            _isHome = true;
        }
    }
    /// <summary>
    /// Used to set a navmesh destination
    /// </summary>
    /// <param name="position"></param>
    void GoToPosition(Vector3 position)
    {
        _navMeshAgent.SetDestination(position);
    }

    /// <summary>
    /// Goes back to the start position
    /// </summary>
    void GoHome()
    {
        GoToPosition(_home);
        goingHome = true;
    }
    /// <summary>
    /// Re-activates the random movement of the plane
    /// </summary>
    void OnFreeMovement()
    {
        _navMeshAgent.ResetPath();
        goingHome = false;
        _isHome = false;
    }

    /// <summary>
    /// Finds a random position on the navmesh and goes there
    /// </summary>
    void GoToRandomPosition()
    {
        GoToPosition(RandomNavMeshLocation(_randomRadius));
    }


    /// <summary>
    /// Get a random position from the nav mesh
    /// </summary>
    /// <param name="radius"></param>
    /// <returns></returns>
    private Vector3 RandomNavMeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitCircle * radius;
        randomDirection += transform.position;

        NavMeshHit hit;
        Vector3 finalPos = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPos = hit.position;
        }
        else
        {
            return transform.position;
            Debug.LogError("Did not hit navmesh");
        }
        return finalPos;
    }
}
